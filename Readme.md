Load automatons from JFLAP and traverse it with test strings.  
The program reads the .jff file, loads an automata out of it, then loads the txt file with the test strings and traverses the automaton.  
A result file is created (called "RESULTADO.txt") where it prints the tested string and its result.  
Usage example:  
CompiledExe "Source.jff" "To test.txt"  
There are 2 included .jff and 2 .txt files to test.  
