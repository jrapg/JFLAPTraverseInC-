/* Esta clase estado es la que (en unión con otros objetos
 * tipo estado) formará un autómata, cada objeto tiene una
 * lista de objetos tipo transicion, los cuales "conectan"
 * los estados entre sí.
*/
#ifndef ESTADO_H_
#define ESTADO_H_
#include "Transicion.h"
#include <vector>
class Estado{
    private:
        vector<Transicion*> * transiciones;
        int id;
        bool esFinal;
    public:
        Estado();
        Estado(int);
        Estado(int, bool);
        void asginaID(int);
        void asignaEsFinal(bool);
        void agregaTransicion(Transicion*);
        void agregaTransicion(int, string);
        int obtieneID();
        bool obtieneEsFinal();
        void imprimeTransiciones();
        vector<int> buscaPosiblesDestinos(char);
        ~Estado();
};
#endif