/* La clase transicion conecta dos estados entre si, 
 * por ejemplo, el estado q0 se conecta al estado q1 
 * con los valores abc, entonces:
 * q0.agregaTransicion("q1", "abc");
*/
#ifndef TRANSICION_H_
#define TRANSICION_H_
#include <iostream>
using namespace std;

class Transicion{
    private:
        string posibleValor;
        int destino;
    public:
        Transicion();
        Transicion(int, string);
        void asignaDestino(int);
        void asignaPosibleValor(string);
        int obtieneDestino();
        string obtienePosibleValor();
        ~Transicion();    
};
#endif