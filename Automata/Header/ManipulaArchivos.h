#ifndef MANIPULAARCHIVOS_H_
#define MANIPULAARCHIVOS_H_

#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <string.h>
#include "../cpp/rapidxml.hpp"
#include "Estado.h"

using namespace std;
using namespace rapidxml;

class ManipulaArchivos{
    public:
        ifstream lector;
        ofstream escritor;
        ManipulaArchivos();
        bool abreArchivo(string, bool);
        vector<Estado*> * parseaXMLPorNombresNodos(string);
        Estado * buscaEnLista(vector<Estado*>*, int);
        string cualValor(string);
        ~ManipulaArchivos();
};
#endif