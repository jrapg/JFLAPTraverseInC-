#ifndef AUTOMATA_H_
#define AUTOMATA_H_
#include "Estado.h"
#include "ManipulaArchivos.h"

class Automata{
    private:        
        static const string numeral, numeralConCero, cero, guion;
        vector<Estado*> * todosEstados;
        //list<bool> recorridos;
        int exitosos, fallidos;
        
    public:
        Automata();          
        void asignaVector(vector<Estado*>*);
        void viajaPorAutomata(string, int);
        void viajaPorAutomata(char, int, vector<int>*);
        void incrementaExitosos();
        void incrementaFallidos();
        void reiniciaExitosos();
        void reiniciaFallidos();
        void asignaExitosos(int);
        void asignaFallidos(int);
        int obtieneExitosos();
        int obtieneFallidos();
        bool leeYRecorreAutomata(ManipulaArchivos &manipulaArchivos, char nombreArchivo[]);
        void imprimeEstadosConTransiciones();
        void imprimeEstadosFinales();
        string cualValor(string);
        Estado * buscaEnLista(int);
        bool contieneLista(int, vector<int>*);
        ~Automata();  
};
#endif