#include "../Header/ManipulaArchivos.h"

ManipulaArchivos::ManipulaArchivos(){}

bool ManipulaArchivos::abreArchivo(string nombreArchivo, bool lecturaOEscritura){
    try{
        if(lecturaOEscritura){
            if(ifstream(nombreArchivo)){   
                this->lector.open(nombreArchivo);
                return true;
            }
            return false;
        }
        else{
            this->escritor.open(nombreArchivo);
            return true;
        }
    }catch(int e){
        return false;
    }
}

vector<Estado*> * ManipulaArchivos::parseaXMLPorNombresNodos(string nombreArchivo){
   if(ifstream(nombreArchivo)){
        vector<Estado*> * resultado = new vector<Estado*>();
        xml_attribute<> *id;
        stringstream buffer;
        bool incialOFinal = false;
        xml_document<> documento;    
        ifstream lectorTemporal(nombreArchivo); 
        buffer << lectorTemporal.rdbuf();
        lectorTemporal.close();
        string contenido(buffer.str());
        documento.parse<0x1>(&contenido[0]);
        xml_node<> * raiz = documento.first_node("structure")->first_node("automaton");
        xml_node<> * nodo = raiz->first_node("state"), * nodoFinal = raiz->last_node("state");

        //Se cargan los estados y se asignan al vector
        while(true){
            id=nodo->first_attribute("id");

            if(nodo->last_node("final")){
                incialOFinal = true;
            }
            Estado * temporal = new Estado(atoi(id->value()), incialOFinal);
            resultado->push_back(temporal);
            if(nodo!=nodoFinal){
                nodo = nodo->next_sibling();
                incialOFinal = false;
            }else{break;}
            
        }
        //Para evitar volver a abrir el archivo y tardar más, se decide continuar con las transiciones inmediatamente

        
        nodo = raiz->first_node("transition");
        nodoFinal = raiz->last_node("transition");
        
        while(true){
            xml_node<> *de, *hacia, *valor;
            Estado * estado;
            
            de=nodo->first_node("from");       
            hacia = de->next_sibling();
            valor = hacia->next_sibling();
            
            estado = buscaEnLista(resultado, atoi(de->value()));
            estado->agregaTransicion(atoi(hacia->value()), 
            cualValor(valor->value()));
            
            if(nodo!=nodoFinal){
                nodo = nodo->next_sibling();}
            else{break;}
        }
        return resultado;
    }
    return NULL;
}

Estado * ManipulaArchivos::buscaEnLista(vector<Estado*> * vector, int id){
    Estado * temporal;
    if(id > vector->size()){
        return temporal;
    }
    return vector->at(id);
}

string ManipulaArchivos::cualValor(string valor){
    if(valor == "[0-9]"){
        return "0123456789";
    }
    if(valor == "[1-9]"){
        return "123456789";
    }
    if(valor==""){
        return " ";
    }
    return valor;
}
ManipulaArchivos::~ManipulaArchivos(){}